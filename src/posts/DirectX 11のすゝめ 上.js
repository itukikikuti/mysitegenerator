import FileSystem from "fs";
import React from "react";
import ReactDOMServer from "react-dom/server";
import EPUB from "epub-gen";
import Kindle from "kindlegen";

export default class Book extends React.Component {
    render() {
        const style = {
            "body": {
                "text-align": "left"
            },
            "code": {
                "font-size": "0.9em",
                "font-family": "monospace, serif"
            },
            "pre": {
                "border": "solid thin",
                "padding": "0.25em"
            },
            "p code": {
                "border": "solid thin",
                "padding": "0.1em"
            },
            ".normal": {
                "opacity": "0.5"
            },
            ".diff": {
                "font-weight": "bold"
            },
            ".ellipsis": {
                "border": "solid",
                //"opacity": "0.25"
            }
        };

        let css = "";
        for (let selector in style) {
            css += selector + "{";
            for (let property in style[selector]) {
                css += property + ":" + style[selector][property] + ";";
            }
            css += "}";
        }

        let epub = {
            title: this.props.page.title,
            author: "菊池 いつき",
            publisher: "windblow",
            cover: "src/static/cover.jpg",
            tocTitle: "目次",
            content: [],
            css: css
        };

        let index = [];
        let contents = [];
        for (let post of this.props.site.posts) {
            index.push(
                <li><a href={"#" + post.title}>{post.title}</a></li>
            );

            contents.push(
                <section>
                    <h1 id={post.title}>{post.title}</h1>
                    {post.content}
                </section>
            );

            let html = ReactDOMServer.renderToStaticMarkup(post.content);
            html = html.replace(/(<code)(>)/g, `$1 style="font-family:monospace"$2`);
            html = html.replace(/(<img src=")([\S\s]*?")/g, "$1file://C:/Users/itukikikuti/Desktop/MySiteGenerator/src$2");

            epub.content.push({
                title: post.title,
                data: html
            });
        }

        new EPUB(epub, "./public" + this.props.page.url.replace(".html", ".epub")).promise.then(
            () => {
                Kindle(FileSystem.readFileSync("./public" + this.props.page.url.replace(".html", ".epub")), (error, mobi) => {
                    //console.log(error + "," + mobi);
                    FileSystem.writeFileSync("./public" + this.props.page.url.replace(".html", ".mobi"), mobi);
                });
            },
            error => {
                console.log(error);
            }
        );

        return (
            <html>
                <head>
                    <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                    <title>{this.props.page.title}</title>
                    <style dangerouslySetInnerHTML={{ __html: css }} />
                </head>
                <body>
                    <h1>{this.props.page.title}</h1>
                    <section>
                        <h1>目次</h1>
                        <ul>{index}</ul>
                    </section>
                    {contents}
                </body>
            </html>
        );
    }
}
