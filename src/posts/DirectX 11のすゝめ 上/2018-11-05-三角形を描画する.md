---
layout: post
title: 三角形を描画する
category: DirectX11
date: 2018-11-05 00:00:01
---

やっと描画できるようになったのでまずは一番簡単に描画できる三角形を描画してみましょう。まず三角形と言うのは頂点で表すので頂点の構造体を作ります。

``` cpp
#include "Library.h"

@d-
struct Vertex
{
    DirectX::XMFLOAT3 position;

    Vertex(DirectX::XMFLOAT3 position)
    {
        this->position = position;
    }
};
-@

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
...
```

頂点は座標の情報を持っています。さっき作ったインプットレイアウトとそっくり同じにしないといけません。この頂点を使って三角形を作ります。

``` cpp
    ...
    Shader& shader = Shader::GetDefault();

@d-
    std::vector<Vertex> vertices
    {
        Vertex(DirectX::XMFLOAT3(0.0f, 0.5f, 0.0f)),
        Vertex(DirectX::XMFLOAT3(0.5f, -0.5f, 0.0f)),
        Vertex(DirectX::XMFLOAT3(-0.5f, -0.5f, 0.0f))
    };
-@

    while (UpdateLibrary())
    ...
```

こんな感じで頂点を3つ作れば三角形になります(直観的ですね)。座標は上、右下、左下という感じにしてます。これを描画するには頂点バッファと言う物を作ります。ID3D11Device::CreateBuffer関数で頂点バッファを作って、ID3D11DeviceContext::IASetVertexBuffers関数で使う頂点バッファを設定して、ID3D11DeviceContext::Draw関数で描画します。

``` cpp
        ...
        Vertex(DirectX::XMFLOAT3(-0.5f, -0.5f, 0.0f))
    };

@d-
    D3D11_BUFFER_DESC vertexBufferDesc = {};
    vertexBufferDesc.ByteWidth = sizeof(Vertex) * (UINT)vertices.size();
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

    D3D11_SUBRESOURCE_DATA vertexSubresourceData = {};
    vertexSubresourceData.pSysMem = vertices.data();

    Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer = nullptr;
    Graphics::GetDevice().CreateBuffer(&vertexBufferDesc, &vertexSubresourceData, vertexBuffer.GetAddressOf());
-@

    while (UpdateLibrary())
    {
        camera->Start();

        shader.Attach();
@d-
        UINT stride = sizeof(Vertex);
        UINT offset = 0;
        Graphics::GetContext().IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);

        Graphics::GetContext().IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        Graphics::GetContext().Draw((UINT)vertices.size(), 0);
-@

        camera->Stop();
    }
    ...
```

こんな感じです。ID3D11DeviceContext::IASetPrimitiveTopologyと言う関数がありますが、D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELISTを指定して頂点の配列を三角形のリストとして解釈させています。他にも点とか線とか三角形とか連なった三角形とかいろいろあります(詳しくは「D3D11_PRIMITIVE_TOPOLOGY」でググってみてください)。実行してみましょう。黒い三連せ、おっと間違えてしまいました。黒い三角形が描画できましたね。

![](/static/5-01.jpg)

では、これをクラスにしてみましょう。Mesh.hとMesh.cppというファイルを作ってください。サンプルは1-05-DrawTriangleフォルダなので、書き写してみましょう。

……………

出来たみたいなんで、Library.hとSource.cppも変えましょう。

``` cpp
...
#include "Camera.h"
@d#include "Mesh.h"

#pragma comment(lib, "d3d11.lib")
...
```

``` cpp
#include "Library.h"

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    Initialize();

    auto camera = std::make_unique<Camera>();
    @dauto mesh = Mesh::CreateTriangle();

    while (Refresh())
    {
        camera->Start();
        @dmesh->Draw();
        camera->Stop();
    }

    return 0;
}
```

MeshクラスのメンバにShaderクラスを追加したので、Mesh::Draw関数の中でShader::Attach関数を使ってシェーダを適用しています。こうすることで複数のメッシュがある時に、別々のシェーダを設定する事も出来ます。
