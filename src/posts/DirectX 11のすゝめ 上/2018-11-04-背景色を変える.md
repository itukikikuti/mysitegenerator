---
layout: post
title: 背景色を変える
category: DirectX11
date: 2018-11-04 00:00:01
---

やっとDirect3D 11が使えるようになったので、何か描画したいですね。それにはまだ必要な準備があります。レンダーターゲットと言う物を作らないといけません。レンダーターゲットを作ればそこに何かを描画したり、レンダーターゲットをクリアする時に背景色を指定したり出来ます。レンダーターゲットを作るのはID3D11Device::CreateRenderTargetView関数で出来ます。そして、ID3D11DeviceContext::OMSetRenderTargets関数で使うレンダーターゲットを指定して、ID3D11DeviceContext::ClearRenderTargetView関数でレンダーターゲットをクリアします。試しにSource.cppに書いてみましょう。

``` cpp
    ...
    InitializeLibrary();

@d-
    Microsoft::WRL::ComPtr<ID3D11Texture2D> texture = nullptr;
    Graphics::GetSwapChain().GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(texture.GetAddressOf()));

    Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTargetView = nullptr;
    Graphics::GetDevice().CreateRenderTargetView(texture.Get(), nullptr, renderTargetView.GetAddressOf());
-@

    while (UpdateLibrary())
    {
@d-
        Graphics::GetContext().OMSetRenderTargets(1, renderTargetView.GetAddressOf(), nullptr);

        float clearColor[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
        Graphics::GetContext().ClearRenderTargetView(renderTargetView.Get(), clearColor);
-@
    }
    ...
```

こんな感じです。画面を管理しているスワップチェーンからIDXGISwapChain::GetBuffer関数で画面のバッファをもらいます。もらったバッファを元にレンダーターゲットを作ります。`float clearColor[4] = { 1.0f, 0.0f, 0.0f, 1.0f };`というのは赤、緑、青、透明度の順番で並んでいて、青にしたいときは`{ 0.0f, 0.0f, 1.0f, 1.0f }`、黄色にしたいときは`{ 1.0f, 1.0f, 0.0f, 1.0f }`にします。こういうのをRGBAといったりします。ここではRが1、Gが0、Bが0なんで赤になります。実行してみましょう。赤い部屋みたいになったらOKです。

![](/static/3-01.jpg)

これを踏まえてCameraクラスを作ってみましょう。なぜカメラなのかは画面のクリアは大抵カメラがするからです。では、Camera.hとCamera.cppというファイルを作ってから、サンプルの1-03-ClearScreenフォルダを見ながら写してみましょう。

……………

出来たようなので、Library.hとSource.cppも変えましょう。

``` cpp
...
#define OEMRESOURCE

@d#include <memory>
#include <vector>
...
}

@d#include "Camera.h"

#pragma comment(lib, "d3d11.lib")
```

``` cpp
#include "Library.h"

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    InitializeLibrary();

@d-
    auto camera = std::make_unique<Camera>();
    camera->color = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
-@

    while (UpdateLibrary())
    {
@d-
        camera->Start();
        // ここで描画
        camera->Stop();
-@
    }

    return 0;
}
```

WindowクラスやGraphicsクラスはstaticな関数しかないクラスでしたが、Cameraクラスはオブジェクトとして使う感じになってるので、もちろん`Camera* camera = new Camera();`みたいにすれば動的に作れます。が、deleteを忘れるとメモリリークを起こすので`auto camera = std::make_unique<Camera>();`というふうにして、std::unique_ptrで管理しています。std::unique_ptrはC++の標準のスマートポインタです(ものすごく有能です)。Camera::Start関数とCamera::Stop関数の間で描画、みたいに使います。Camera::Stop関数は何もしてない関数ですが、今後描画した後になにかしたいときはここに追記していきます。例えばポストエフェクトとかですね。
