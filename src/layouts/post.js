import React from "react";
import Moment from "moment";
import Default from "./default";

export default class Post extends React.Component {
    render() {
        let moment = Moment(this.props.page.date);
        return (
            <Default site={this.props.site} page={this.props.page}>
                <article>
                    <h1>{this.props.page.title}</h1>
                    <time>{moment.utc().format("YYYY-MM-DD HH:mm:ss")}</time>
                    {this.props.children}
                </article>
            </Default>
        );
    }
}
