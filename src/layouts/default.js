import React from "react";

export default class Default extends React.Component {
    render() {
        const style = {
            "img": {
                "width": "100%"
            },
            "em": {
                "-webkit-text-emphasis": "filled dot",
                "text-emphasis": "filled dot"
            }
        };

        let css = "";
        for (let selector in style) {
            css += selector + "{";
            for (let property in style[selector]) {
                css += property + ":" + style[selector][property] + ";";
            }
            css += "}";
        }

        return (
            <html lang="ja" prefix="og: http://ogp.me/ns#">
                <head>
                    <meta charset="UTF-8" />
                    <meta name="theme-color" content="#06F" />
                    <meta name="viewport" content="width=device-width,initial-scale=1" />
                    <title>{this.props.page.title} | windblow</title>
                    <style dangerouslySetInnerHTML={{ __html: css }} />
                </head>
                <body>
                    <header>
                        <h1><a href="/">windblow</a></h1>
                        <small>© 2016 windblow</small>
                        <nav>
                            <ul>
                                <li><a href="/">トップ</a></li>
                                <li><a href="/games/StickyCat/">Sticky Cat</a></li>
                                <li><a href="/posts/">投稿</a></li>
                            </ul>
                        </nav>
                    </header>
                    <main>{this.props.children}</main>
                </body>
            </html>
        );
    }
}
