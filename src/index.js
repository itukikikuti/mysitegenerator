import React from "react";
import Default from "./layouts/default";

export default class Post extends React.Component {
    render() {
        let contents = [];
        for (let post of this.props.site.posts) {
            contents.push(
                <li><a href={post.url}>{post.title}</a></li>
            );
        }
        return (
            <Default site={this.props.site} page={this.props.page}>
                <ul>
                    {contents}
                </ul>
            </Default>
        );
    }
}
