#!/usr/bin/env node
const FileSystem = require("fs");
const RootPath = require("app-root-path");
const Path = require("path");

function GetPath(path) {
    return Path.normalize(RootPath + path);
}
let list = FileSystem.readdirSync(GetPath("/"));
console.log(list);
