import FileSystem from "fs-extra";
import Path from "path";
import React from "react";
import ReactDOMServer from "react-dom/server";
import Remarkable from "remarkable";
import YAML from "js-yaml";
import Pretty from "pretty";
import Post from "./src/layouts/post";

const remarkable = new Remarkable({
    html: true,
    linkify: true
});

class Page {
    constructor() {
        this.title;
        this.content;
        this.url;
        this.date;
    }
}

class Site {
    constructor() {
        let postPaths = [];
        this.GetFilePathList("./src/", ".md", postPaths);

        this.posts = this.LoadPosts(postPaths, ".md");
        this.Sort(this.posts);

        let pagePaths = [];
        this.GetFilePathList("./src/", ".js", pagePaths);

        this.pages = this.LoadPages(pagePaths, ".js");
        this.Sort(this.pages);
    }

    GetFilePathList(path, extname, list) {
        let pathNames = FileSystem.readdirSync(path);

        for (let pathName of pathNames) {
            if (FileSystem.statSync(path + pathName).isDirectory()) {
                this.GetFilePathList(path + pathName + "/", extname, list);
            } else {
                if (Path.extname(pathName) == extname) {
                    list.push(path + pathName);
                }
            }
        }
    }

    LoadPosts(pathList, extname) {
        let posts = [];
        for (let path of pathList) {
            let post = new Page();
            let fileInfo = Path.basename(path, extname).split("-");
            post.title = fileInfo[fileInfo.length - 1];

            let markdown = FileSystem.readFileSync(path, "utf8");
            let yaml;
            markdown = markdown.replace(/---([\s\S]*)---/, (string, match) => {
                yaml = match;
                return "";
            });

            let html = remarkable.render(markdown);
            html = html.replace(/<pre><code class="language-cpp">([\S\s]*?)<\/code><\/pre>/g, `<pre><code class="language-cpp"><span class="normal">$1</span></code></pre>`);
            html = html.replace(/@d-\n?([\S\s]*?)\n?-@/g, `</span><span class="diff">$1</span><span class="normal">`);
            html = html.replace(/@d([\S\s]*?\n)/g, `</span><span class="diff">$1</span><span class="normal">`);
            html = html.replace(/(\.\.\.)/g, `</span><span class="ellipsis">$1</span><span class="normal">`);

            post.content = <div dangerouslySetInnerHTML={{ __html: html }} />;
            post.url = (Path.dirname(path.replace(/^\.\/src/g, "")) + "/" + post.title + ".html").replace(/\s/g, "_");
            post.date = new Date(`${fileInfo[0]}-${fileInfo[1]}-${fileInfo[2]}T00:00:00Z`);

            let frontMatter = YAML.safeLoad(yaml);
            for (let key in frontMatter) {
                post[key] = frontMatter[key];
            }
            
            posts.push(post);
        }

        return posts;
    }

    LoadPages(pathList, extname) {
        let pages = [];
        for (let path of pathList) {
            let page = new Page();
            page.title = Path.basename(path, extname);
            page.url = (Path.dirname(path.replace(/^\.\/src/g, "")) + "/" + page.title + ".html").replace(/\s/g, "_");
            page.date = new Date("0000-00-00T00:00:00Z");
            page.content = require(path).default;
            pages.push(page);
        }

        return pages;
    }

    Sort(pages) {
        pages.sort((a, b) => {
            if (a.date < b.date) return -1;
            if (a.date > b.date) return 1;
            return 0;
        });
    }
}

class Staticgen {
    constructor() {
        this.site = new Site();

        for (let post of this.site.posts) {
            this.GeneratePosts(post);
        }

        for (let page of this.site.pages) {
            this.GeneratePages(page);
        }

        FileSystem.copySync("./src/static", "./public/static");
    }
    
    GeneratePosts(page) {
        let htmlSource = Pretty("<!DOCTYPE html>" + ReactDOMServer.renderToStaticMarkup(<Post site={this.site} page={page}>{page.content}</Post>));
        FileSystem.mkdirsSync(Path.dirname("./public" + page.url));
        FileSystem.writeFileSync("./public" + page.url, htmlSource, "utf8");
    }

    GeneratePages(page) {
        let htmlSource = Pretty("<!DOCTYPE html>" + ReactDOMServer.renderToStaticMarkup(<page.content site={this.site} page={page} />));
        FileSystem.mkdirsSync(Path.dirname("./public" + page.url));
        FileSystem.writeFileSync("./public" + page.url, htmlSource, "utf8");
    }
}

new Staticgen();
