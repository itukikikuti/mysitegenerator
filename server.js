import HTTP from "http";
import Connect from "connect";
import Logger from "morgan";
import ServeStatic from "serve-static";

const app = Connect().use(Logger("dev")).use(ServeStatic(process.cwd() + "/public"));
HTTP.createServer(app).listen(80);
