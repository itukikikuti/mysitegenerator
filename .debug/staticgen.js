"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.site = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Site = function () {
    function Site() {
        _classCallCheck(this, Site);

        this.posts = [];

        var postPaths = this.GetPagePathList("./src/", ".md");
        console.log(postPaths.length);
    }

    _createClass(Site, [{
        key: "GetPagePathList",
        value: function GetPagePathList(path, extname) {
            var list = [];
            var pathNames = _fs2.default.readdirSync(path);

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = pathNames[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var pathName = _step.value;

                    if (_fs2.default.statSync(path + pathName).isDirectory()) {
                        list.concat(this.GetPagePathList(path + pathName + "/", extname));
                    } else {
                        if (_path2.default.extname(pathName) == extname) {
                            list.push(path + pathName);
                        }
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            console.log(path + ":" + list);

            return list;
        }
    }]);

    return Site;
}();

var Page = function Page() {
    _classCallCheck(this, Page);

    this.content;
    this.title;
    this.url;
    this.date;
};

var site = exports.site = new Site();

/*
let moment = Moment("2018-11-25 16:38:24");
console.log(moment.toString());

if (!FileSystem.existsSync("./public/")) {
    FileSystem.mkdirSync("./public/");
}

let bookNames = FileSystem.readdirSync("./src/posts/");

for (let bookName of bookNames) {
    let path = `./src/posts/${bookName}/`;
    let html = "<!DOCTYPE html>" + ReactDOMServer.renderToStaticMarkup(<Book path={path}></Book>);

    FileSystem.writeFileSync(`./public/${bookName}.html`, html, "utf8");
}

function SearchDirectory(path) {
    let names = FileSystem.readdirSync(path);

    for (let name of names) {
        if (FileSystem.statSync(path + name).isDirectory()) {
            let outputPath = (path + name).replace("src", "public").replace(/\s/g, "_");
            if (!FileSystem.existsSync(outputPath)) {
                FileSystem.mkdirSync(outputPath);
            }

            SearchDirectory(path + name + "/");
        } else {
            if (Path.extname(name) == ".md") {
                let html = "<!DOCTYPE html>" + ReactDOMServer.renderToStaticMarkup(<Post path={path + name} />);
                let outputPath = (path + name).replace("src", "public").replace(".md", ".html").replace(/\s/g, "_");

                FileSystem.writeFileSync(outputPath, html, "utf8");
            }
        }
    }
}

SearchDirectory("./src/");
*/
//# sourceMappingURL=staticgen.js.map