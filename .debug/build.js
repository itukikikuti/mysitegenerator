#!/usr/bin/env node
"use strict";

var FileSystem = require("fs");
var RootPath = require("app-root-path");
var Path = require("path");

function GetPath(path) {
    return Path.normalize(RootPath + path);
}
var list = FileSystem.readdirSync(GetPath("/"));
console.log(list);
//# sourceMappingURL=build.js.map