"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _remarkable = require("remarkable");

var _remarkable2 = _interopRequireDefault(_remarkable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var remarkable = new _remarkable2.default({
    html: true,
    linkify: true
});

var Post = function (_React$Component) {
    _inherits(Post, _React$Component);

    function Post() {
        _classCallCheck(this, Post);

        return _possibleConstructorReturn(this, (Post.__proto__ || Object.getPrototypeOf(Post)).apply(this, arguments));
    }

    _createClass(Post, [{
        key: "render",
        value: function render() {
            var markdown = _fs2.default.readFileSync(this.props.path, "utf8");
            var html = remarkable.render(markdown);

            return _react2.default.createElement(
                "html",
                null,
                _react2.default.createElement(
                    "head",
                    null,
                    _react2.default.createElement("meta", { httpEquiv: "Content-Type", content: "text/html; charset=utf-8" }),
                    _react2.default.createElement(
                        "title",
                        null,
                        _path2.default.basename(this.props.path, ".md").split("-")[6]
                    )
                ),
                _react2.default.createElement(
                    "body",
                    null,
                    _react2.default.createElement(
                        "h1",
                        null,
                        _path2.default.basename(this.props.path, ".md").split("-")[6]
                    ),
                    _react2.default.createElement("div", { dangerouslySetInnerHTML: { __html: html } })
                )
            );
        }
    }]);

    return Post;
}(_react2.default.Component);

exports.default = Post;
//# sourceMappingURL=post.js.map