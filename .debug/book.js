"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _remarkable = require("remarkable");

var _remarkable2 = _interopRequireDefault(_remarkable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var remarkable = new _remarkable2.default({
    html: true,
    linkify: true
});

var Book = function (_React$Component) {
    _inherits(Book, _React$Component);

    function Book() {
        _classCallCheck(this, Book);

        return _possibleConstructorReturn(this, (Book.__proto__ || Object.getPrototypeOf(Book)).apply(this, arguments));
    }

    _createClass(Book, [{
        key: "render",
        value: function render() {
            var style = {
                "body": {
                    "text-align": "left"
                },
                "h1": {
                    "page-break-before": "always"
                },
                "code": {
                    "font-size": "0.9em"
                },
                "pre": {
                    "margin-top": "0",
                    "margin-bottom": "0"
                }
            };

            var css = "";
            for (var selector in style) {
                css += selector + "{";
                for (var property in style[selector]) {
                    css += property + ":" + style[selector][property] + ";";
                }
                css += "}";
            }

            var postNames = _fs2.default.readdirSync(this.props.path);
            var contents = [];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = postNames[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var postName = _step.value;

                    var path = _path2.default.parse(this.props.path + postName);
                    var markdown = _fs2.default.readFileSync(_path2.default.format(path), "utf8");
                    var html = remarkable.render(markdown);
                    html = html.replace(/<p><small>/g, "<small>").replace(/<\/small><\/p>/g, "</small>");

                    contents.push(_react2.default.createElement(
                        "section",
                        null,
                        _react2.default.createElement(
                            "h1",
                            null,
                            path.name.split("-")[6]
                        ),
                        _react2.default.createElement("div", { dangerouslySetInnerHTML: { __html: html } })
                    ));
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return _react2.default.createElement(
                "html",
                null,
                _react2.default.createElement(
                    "head",
                    null,
                    _react2.default.createElement("meta", { httpEquiv: "Content-Type", content: "text/html; charset=utf-8" }),
                    _react2.default.createElement(
                        "title",
                        null,
                        _path2.default.basename(this.props.path)
                    ),
                    _react2.default.createElement("style", { dangerouslySetInnerHTML: { __html: css } })
                ),
                _react2.default.createElement(
                    "body",
                    null,
                    _react2.default.createElement(
                        "h1",
                        null,
                        _path2.default.basename(this.props.path)
                    ),
                    contents
                )
            );
        }
    }]);

    return Book;
}(_react2.default.Component);

exports.default = Book;
//# sourceMappingURL=book.js.map